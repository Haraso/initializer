Initializer.js
==========


**Initializer** is a simple usesable library. It can initialize and preload components by async with dependencies. This can solve the problem, which component loads first and what will be next.

----------

Useage
----------

It can be use in browser and node as well, becouse **Initializer** automaticly added to global scope.
   
**browser:**
    
```
<script type="text/javascript" src="initializer.js"></script>
```
    
**node:**
    
```
require("initializer");
```
>**!!!** do not need equal require, becouse **Initializer** added to global scope

Functions
--------------

####  **.config( void | string | Object )**
   
Use to configurate the **Initializer**

```arg: string```

It can be *"debug"* to log all action what **Initializer** do, or *"product"* if want only error logs. 
If it leave blank same as *"product"*.
   
```arg: Object```

```
{
	logLevel: number
}
```

*logLevel* can be a number between **-1** and **3**

```logLevel: -1``` **->** log: nothing

```logLevel: 0``` **->** log: only errors

```logLevel: 1``` **->** log: warn, error

```logLevel: 2``` **->** log: log, warn, error

```logLevel: 3``` **->** log: info, log, warn, error

#### **.init( void | string )** 

Use to init a new component.

>**!!!** The component is defined as long as it does not use .init again

```arg: void```

Auto generate ID for the component.

 >**!!!!** use only if dont want to know what is the component *ID*

```arg: string```

ID of the component

 >**!!!!** *ID* need to be unique

#### **.deps( ...string )** 

Dependencies for the component.

```arg: string```

ID / IDs of the depended component / components

>**!!!** The order of the IDs is importent to get back deps correctly

#### **.component( ( ...deps?, done, err, log) => void )** 

Use to define component.

```arg: ( ...deps?, done, err, log) => void```

Argument is a function, thet call at when initializeing.

```cb_arg: log```

Log function. Use if want.

```log(type: "error" | "warn" | "log" | "info", text: string)```

```cb_arg: err```

Err function. Call if something wrong or want to jump alter. If defined alter component the argument of *err( err_arg )* will be logged like warning in console.

```err( void | any )```

>**!!!** If not found alter component **Initializer** will be throw *err_arg* what come from *err( err_arg )*
  
```cb_arg: done```

Done function. Call if component has been initialized.

```done( void | any )```

>**!!!** *done( done_arg )* done_arg is what other components come from if they marked the current component in deps

```cb_arg: ...deps```

Deps are arguments what it come from marked components. It comes ordered by the same as defined in *.deps( )*

#### **.alter( ( ...deps?, done, err, log) => void )** 

Alter is a same as *.components( )*. It call if something wrong in original component. Define many of alter does need or nothing.

>**!!!** The alternatives will be selected in the order of their definition.

#### **.start( void )**

Start initializeing.

>**!!!** If is started do not try start again. Will be in next version.

Preloading
----------------

**Initializer** already has a preloader.

#### **.preload( filePath: string, type: string )** 

It can be preload files.

>**!!!** If the file is *js* doesent need *type*.

-------------------

Examples
--------------

#### **example - basic - src:**
```
initializer

.init("BComponent")
.deps("AComponent")
.component(function(A, done){
	console.log("B");
	done({hello: A.hello+"B"})
})

.init("AComponent")
.component(function(done){
	setTimeout(function(){
		console.log("A");
		done({hello: "A"});
	}, 1000)
})

.init("CComponent")
.deps("AComponent", "BComponent")
.component(function(A, B, done){
	console.log("C");
	console.log("A say:", A.hello);
	console.log("B say:", B.hello);
	done();
})

.start()
```
#### **example - basic - output:**
```
A
B
C
A say: A
B say: AB
```
#### **example - error - src:**
```
initializer

.init("a")
.deps("c")
.component(function(c, done){
    setTimeout(
        function(){
            console.log("a", c+1);
            done(c+1);
        },
        Math.random()*1000
    )
})

.init("b")
.deps("a")
.component(function(done){
    setTimeout(
        function(){
            console.log("b", 1);
            done(1);
        },
        Math.random()*1000
    )
})

.init("c")
.deps("b")
.component(function(b, done){
    setTimeout(
        function(){
            console.log("c", b+1);
            done(b+1);
        },
        Math.random()*1000
    )
})


.start();
```
#### **example - error - output:**
```
Uncaught Initializer: found circle (a) -> c -> b -> (a)
```
#### **example - alter - src:**
```
initializer.config({logLevel: 1})

.init("e")
.deps("a", "b", "c")
.component(function(a, b, c, done, err, log){
    setTimeout(
        function(){
            err("Fake error");
        },
        Math.random()*1000
    )
})
.alter(function(a, b, c, done, err){
    setTimeout(
        function(){
            console.log("alter", "e", a+b+c+5);
            done(5);
        },
        Math.random()*1000
    )
})


.init("d")
.component(function(done, err){
    setTimeout(
        function(){
            console.log("d", 4);
            done(4);
        },
        Math.random()*1000
    )
})

.init("c")
.deps("b")
.component(function(b, done, err){
    setTimeout(
        function(){
            console.log("c", b+2);
            done(3);
        },
        Math.random()*1000
    )
})

.init("b")
.deps("a")
.component(function(a, done, err){
    setTimeout(
        function(){
            console.log("b", a+2);
            done(2);
        },
        Math.random()*1000
    )
})

.init("a")
.component(function(done, err){
    setTimeout(
        function(){
            console.log("a", 1);
            done(1);
        },
        Math.random()*1000
    )
})

.start()
```
#### **example - alter - output:**
```
a 1
b 3
c 4
Initializer: e component hes error try alter
Fake error
alter e 11
d 4
```